import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            list.add(i);
            list.add(i * 3);
        }
        list.set(5, 5);


        List list2 = list.stream().toList();
        System.out.println("исходник");
        System.out.println(list2);
        list2 = list.stream().map(x -> x*2).toList();
        System.out.println("преобразование а map");
        System.out.println(list2);
        list2 = list.stream().limit(5).toList();
        System.out.println("ограничение количества элементов");
        System.out.println(list2);
        list2 = list.stream().distinct().toList();
        System.out.println("удаление одинаковых узлов");
        System.out.println(list2);
        list2 = list.stream().filter(x -> x > 5).toList();
        System.out.println("прохождение фильтра");
        System.out.println(list2);
        list.stream().forEach(System.out::print);
        System.out.println("forEach");
        System.out.println(list2);


        List list3 = MyStream.of(list).toList();
        System.out.println("исходник");
        System.out.println(list3);
        list3 = MyStream.of(list).map(n -> n*2).toList();
        System.out.println(list3);
        list3 = MyStream.of(list).limit(5).toList();
        System.out.println(list3);
        list3 = MyStream.of(list).distinct().toList();
        System.out.println(list3);
        list3 = MyStream.of(list).filter(x -> x > 5).toList();
        System.out.println(list3);
        System.out.println(MyStream.of(list).filter(x -> x > 5).findFirst());
        System.out.println(MyStream.of(list).filter(x -> x > 5).count());
        Map map = MyStream.of(list).filter(x -> x > 5).toMap();
        System.out.println(map);
        MyStream.of(list).forEach(System.out::print);
    }
}
