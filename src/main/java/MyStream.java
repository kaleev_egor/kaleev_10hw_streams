import java.util.*;

public class MyStream {

    public MyStream() {
    }

    private static List<Integer> list;

    public static MyStream of(List<? extends Integer> list) {
        //System.out.println("запись листа в класс");
        MyStream.list = new ArrayList<>(list);
        return new MyStream();
    }

    public MyStream limit(int lim) {
        System.out.println("ограничение количества элементов");
        this.list = list.subList(0, lim);
        return this;
    }

    public MyStream distinct() {
        System.out.println("удаление одинаковых узлов");
        Set<Integer> duplicateSet = new HashSet<>();
        duplicateSet.addAll(list);
        list.clear();
        for (int i : duplicateSet) {
            list.add(i);
        }
        return this;
    }

    public List toList() {
        //System.out.println("преобразование а List");
        return list;
    }

    public Map toMap() {
        System.out.println("преобразование а Map");
        Map<Integer, Integer> map = new HashMap();
        for (int i = 0; i < list.size(); i++) {
            map.put(i, list.get(i));
        }
        return map;
    }

    public Integer findFirst() {
        System.out.println("выполняется findFirst");
        if (!list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public void forEach(IForEach forEach) {
        System.out.println("выполняется forEach");
        for (int i: list) {
            forEach.calculate(i);
        }
    }
    interface IForEach {
        void calculate(int x);
    }

    public int count() {
        System.out.println("количество элементов");
        return list.size();
    }


    public MyStream map(IMap mapper) {
        System.out.println("преобразование а map");
        for (int i = 0; i < list.size(); i++) {
            int k = mapper.calculate(list.get(i));
            list.set(i, k);
        }
        return this;
    }

    interface IMap {
        int calculate(int x);
    }

    public MyStream filter(IFilter mapper) {
        System.out.println("прохождение фильтра");
        List<Integer> resList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            boolean k = mapper.calculate(list.get(i));
            if (k) {
                resList.add(list.get(i)); //был вариант без отдельного массива, удаляя элементы из начальног, но он путал элементы, удалял то по индексу, то по значению
            }
        }
        list = resList;
        return this;
    }

    interface IFilter {
        boolean calculate(int x);
    }
}



